package com.tw.moon.bookstore.models;


public class ApplicationInfo {

    private String name;

    public ApplicationInfo() {
        this.name = "Anirban & Anshul";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
