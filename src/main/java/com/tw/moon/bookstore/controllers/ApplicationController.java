package com.tw.moon.bookstore.controllers;


import com.tw.moon.bookstore.models.ApplicationInfo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApplicationController {

    @GetMapping("/applications")
    public ApplicationInfo index() {
        return new ApplicationInfo();
    }
}
