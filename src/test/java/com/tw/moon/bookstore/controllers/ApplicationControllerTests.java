package com.tw.moon.bookstore.controllers;

import org.junit.Test;

import static org.junit.Assert.*;

public class ApplicationControllerTests {
    @Test
    public void shouldReturnApplicationInfoObject() {
        ApplicationController controller = new ApplicationController();

        assertEquals("Anirban & Anshul",  controller.index().getName());
    }
}
