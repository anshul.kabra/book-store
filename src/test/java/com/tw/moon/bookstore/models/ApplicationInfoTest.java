package com.tw.moon.bookstore.models;

import org.junit.Test;

import static org.junit.Assert.*;

public class ApplicationInfoTest {
    @Test
    public void should_return_correct_book_store() {
        ApplicationInfo applicationInfo = new ApplicationInfo();
        assertEquals("Anirban & Anshul", applicationInfo.getName());
    }
}
