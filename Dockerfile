FROM openjdk:8-alpine

ADD ./build/libs/book-store-0.0.1-SNAPSHOT.jar app.jar

ENTRYPOINT ["java", "-jar", "/app.jar"]